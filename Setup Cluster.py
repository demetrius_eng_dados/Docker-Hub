#!/usr/bin/env python
# coding: utf-8

# In[ ]:


from nlptoolkit.utils.misc import save_as_pickle
import logging
from argparse import ArgumentParser

logging.basicConfig(format='%(asctime)s [%(levelname)s]: %(message)s',                     datefmt='%m/%d/%Y %I:%M:%S %p', level=logging.INFO)
logger = logging.getLogger('__file__')

if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("--train_data", type=str, default="./data/train.csv",                         help="training data csv file path")
    parser.add_argument("--window", type=int, default=10, help='Window size to calculate PMI')
    parser.add_argument("--max_vocab_len", type=int, default=7000, help="GCN encoder: Max vocab size to consider based on top frequency tokens")
    parser.add_argument('--batched', type=int, default=0,                        help= 'For GCN, GIN - 0: no batch training ; 1: Yes')
    parser.add_argument("--hidden_size_1", type=int, default=300, help="Size of first GCN encoder hidden weights")
    parser.add_argument("--batch_size", type=int, default=96, help="Training batch size")
    parser.add_argument("--gradient_acc_steps", type=int, default=1, help="No. of steps of gradient accumulation")
    parser.add_argument("--max_norm", type=float, default=1.0, help="Clipped gradient norm")
    parser.add_argument("--num_epochs", type=int, default=300, help="No of epochs")
    parser.add_argument("--lr", type=float, default=0.003, help="learning rate")
    parser.add_argument("--model_no", type=int, default=0, help='''Model ID: (0: Deep Graph Infomax (DGI)), 
                                                                            ''')
    parser.add_argument("--encoder_type", type=str, default="GIN",                         help="For DGI, the encoder type (GCN, GIN)")
    parser.add_argument("--train", type=int, default=1, help="Train model on dataset")
    parser.add_argument("--infer", type=int, default=1, help="Infer input sentence labels from trained model")
    args = parser.parse_args()
    save_as_pickle("args.pkl", args)
    
    if args.model_no == 0:
        from nlptoolkit.clustering.models.DGI.trainer import train_and_fit
        from nlptoolkit.clustering.models.DGI.infer import infer_from_trained
    
    if args.train == 1:
        output = train_and_fit(args)
        
    if args.infer == 1:
        inferer = infer_from_trained()
        inferer.infer_embeddings()
        
        pca, pca_embeddings = inferer.PCA_analyze(n_components=2)
        tsne_embeddings = inferer.plot_TSNE(plot=True)
        result = inferer.cluster_tsne_embeddings(tsne_embeddings,                                                 n_start=4, n_stop=30, method='ac', plot=True)
        node_clusters = inferer.get_clustered_nodes(result['labels'])


# In[ ]:


from nlptoolkit.utils.misc import save_as_pickle
from nlptoolkit.ASR.trainer import train_and_fit
from nlptoolkit.ASR.infer import infer
import logging
from argparse import ArgumentParser

logging.basicConfig(format='%(asctime)s [%(levelname)s]: %(message)s',                     datefmt='%m/%d/%Y %I:%M:%S %p', level=logging.INFO)
logger = logging.getLogger('__file__')

if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("--folder", type=str, default="train-clean-5", help="Folder containing speech files")
    parser.add_argument("--level", type=str, default="word", help="Level of tokenization (word or char)")
    parser.add_argument("--use_lg_mels", type=int, default=1, help="Use log mel spectrogram if 1, else if 0 use MFCC instead")
    parser.add_argument("--use_conv", type=int, default=1, help="Use convolution on features if 1, else if 0 don't use")
    parser.add_argument("--n_mels", type=int, default=80, help="Number of Mel bands to generate")
    parser.add_argument("--n_mfcc", type=int, default=13, help="number of MFCC coefficients")
    parser.add_argument("--n_fft", type=int, default=25, help="Length of FFT window (ms)")
    parser.add_argument("--hop_length", type=int, default=10, help="Length between successive frames (ms)")
    parser.add_argument("--max_frame_len", type=int, default=1000, help="Max audio frame length") # 3171
    parser.add_argument("--d_model", type=int, default=128, help="Transformer model dimension")
    parser.add_argument("--ff_dim", type=int, default=128, help="Feed forward layer dimension")
    parser.add_argument("--num", type=int, default=6, help="Number of layers")
    parser.add_argument("--n_heads", type=int, default=4, help="Number of attention heads")
    parser.add_argument("--batch_size", type=int, default=30, help="Batch size")
    parser.add_argument("--fp16", type=int, default=1, help="1: use mixed precision ; 0: use floating point 32")
    parser.add_argument("--num_epochs", type=int, default=9000, help="No of epochs")
    parser.add_argument("--lr", type=float, default=0.0003, help="learning rate")
    parser.add_argument("--gradient_acc_steps", type=int, default=4, help="Number of steps of gradient accumulation")
    parser.add_argument("--max_norm", type=float, default=1.0, help="Clipped gradient norm")
    parser.add_argument("--T_max", type=int, default=5000, help="number of iterations before LR restart")
    parser.add_argument("--model_no", type=int, default=0, help="Model ID: 0 = Transformer, 1 = LAS")
    
    parser.add_argument("--train", type=int, default=1, help="Train model on dataset")
    parser.add_argument("--infer", type=int, default=0, help="Infer input sentence labels from trained model")
    args = parser.parse_args()
    save_as_pickle("args.pkl", args)
    
    if args.train:
        train_and_fit(args, pyTransformer=False)
    if args.infer:
        infer(file_path="./data/train-clean-5/19/198/19-198-0008.flac", speaker='19')
        outputs = infer()


# In[ ]:


from nlptoolkit.punctuation_restoration.trainer import train_and_fit
from nlptoolkit.punctuation_restoration.infer import infer_from_trained
from nlptoolkit.utils.misc import save_as_pickle
from argparse import ArgumentParser
import logging

logging.basicConfig(format='%(asctime)s [%(levelname)s]: %(message)s',                     datefmt='%m/%d/%Y %I:%M:%S %p', level=logging.INFO)
logger = logging.getLogger('__file__')

if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("--data_path", type=str, default="./data/train.tags.en-fr.en", help="path to text file")
    parser.add_argument("--level", type=str, default="bpe", help="Level of tokenization (word, char or bpe)")
    parser.add_argument("--bpe_word_ratio", type=float, default=0.7, help="Ratio of BPE to word vocab")
    parser.add_argument("--bpe_vocab_size", type=int, default=7000, help="Size of bpe vocab if bpe is used")
    parser.add_argument("--batch_size", type=int, default=32, help="Batch size")
    parser.add_argument("--d_model", type=int, default=512, help="Transformer model dimension")
    parser.add_argument("--ff_dim", type=int, default=2048, help="Transformer feed-forward layer dimension")
    parser.add_argument("--num", type=int, default=6, help="Number of layers")
    parser.add_argument("--n_heads", type=int, default=8, help="Number of attention heads")
    parser.add_argument("--max_encoder_len", type=int, default=80, help="Max src length")
    parser.add_argument("--max_decoder_len", type=int, default=80, help="Max trg length")
    parser.add_argument("--LAS_embed_dim", type=int, default=512, help="PuncLSTM Embedding dimension")
    parser.add_argument("--LAS_hidden_size", type=int, default=512, help="PuncLSTM listener hidden_size")
    parser.add_argument("--num_epochs", type=int, default=127, help="No of epochs")
    parser.add_argument("--lr", type=float, default=0.00003, help="learning rate")
    parser.add_argument("--gradient_acc_steps", type=int, default=2, help="Number of steps of gradient accumulation")
    parser.add_argument("--max_norm", type=float, default=1.0, help="Clipped gradient norm")
    parser.add_argument("--T_max", type=int, default=5000, help="number of iterations before LR restart")
    parser.add_argument("--model_no", type=int, default=1, help="Model ID - 0: PuncTransformer\n                        1: PuncLSTM\n                        2: pyTransformer")
    
    parser.add_argument("--train", type=int, default=0, help="Train model on dataset")
    parser.add_argument("--infer", type=int, default=1, help="Infer input sentence labels from trained model")
    args = parser.parse_args()
    save_as_pickle("args.pkl", args)
    
    if args.train:
        train_and_fit(args)
    if args.infer:
        inferer = infer_from_trained()
        inferer.infer_from_data()


# In[ ]:


from nlptoolkit.punctuation_restoration.trainer import train_and_fit
from nlptoolkit.punctuation_restoration.infer import infer_from_trained
from nlptoolkit.utils.misc import save_as_pickle
from argparse import ArgumentParser
import logging

logging.basicConfig(format='%(asctime)s [%(levelname)s]: %(message)s',                     datefmt='%m/%d/%Y %I:%M:%S %p', level=logging.INFO)
logger = logging.getLogger('__file__')

if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("--data_path", type=str, default="./data/train.tags.en-fr.en", help="path to text file")
    parser.add_argument("--level", type=str, default="bpe", help="Level of tokenization (word, char or bpe)")
    parser.add_argument("--bpe_word_ratio", type=float, default=0.7, help="Ratio of BPE to word vocab")
    parser.add_argument("--bpe_vocab_size", type=int, default=7000, help="Size of bpe vocab if bpe is used")
    parser.add_argument("--batch_size", type=int, default=32, help="Batch size")
    parser.add_argument("--d_model", type=int, default=512, help="Transformer model dimension")
    parser.add_argument("--ff_dim", type=int, default=2048, help="Transformer feed-forward layer dimension")
    parser.add_argument("--num", type=int, default=6, help="Number of layers")
    parser.add_argument("--n_heads", type=int, default=8, help="Number of attention heads")
    parser.add_argument("--max_encoder_len", type=int, default=80, help="Max src length")
    parser.add_argument("--max_decoder_len", type=int, default=80, help="Max trg length")
    parser.add_argument("--LAS_embed_dim", type=int, default=512, help="PuncLSTM Embedding dimension")
    parser.add_argument("--LAS_hidden_size", type=int, default=512, help="PuncLSTM listener hidden_size")
    parser.add_argument("--num_epochs", type=int, default=127, help="No of epochs")
    parser.add_argument("--lr", type=float, default=0.00003, help="learning rate")
    parser.add_argument("--gradient_acc_steps", type=int, default=2, help="Number of steps of gradient accumulation")
    parser.add_argument("--max_norm", type=float, default=1.0, help="Clipped gradient norm")
    parser.add_argument("--T_max", type=int, default=5000, help="number of iterations before LR restart")
    parser.add_argument("--model_no", type=int, default=1, help="Model ID - 0: PuncTransformer\n                        1: PuncLSTM\n                        2: pyTransformer")
    
    parser.add_argument("--train", type=int, default=0, help="Train model on dataset")
    parser.add_argument("--infer", type=int, default=1, help="Infer input sentence labels from trained model")
    args = parser.parse_args()
    save_as_pickle("args.pkl", args)
    
    if args.train:
        train_and_fit(args)
    if args.infer:
        inferer = infer_from_trained()
        inferer.infer_from_data()

